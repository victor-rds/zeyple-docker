FROM debian:11-slim

ARG TARGETPLATFORM
ARG TARGETARCH
ARG DEBIAN_FRONTEND=noninteractive

ARG S6_OVERLAY_URL="https://github.com/just-containers/s6-overlay/releases/download"
ARG S6_OVERLAY_VERSION=3.1.1.0

RUN set -eux; \
  apt update && apt-get upgrade -y; \
  apt-get install -y --no-install-recommends \
    ca-certificates xz-utils wget; \
  apt-get clean; \
  rm -rf /var/lib/apt/lists/*; \
  echo "Arch: ${TARGETARCH}"; \
  case "${TARGETARCH}" in \
    "amd64") S6_ARCH="x86_64" ;; \
    "arm64") S6_ARCH="aarch64" ;; \
    "arm") S6_ARCH="armhf" ;; \
    "686") S6_ARCH="686" ;; \
    "riscv64") S6_ARCH="riscv64" ;; \
    *) S6_ARCH="486" ;; \
  esac; \
  mkdir /tmp/s6; \
  cd /tmp/s6; \
  wget "${S6_OVERLAY_URL}/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_ARCH}.tar.xz"; \
  wget "${S6_OVERLAY_URL}/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_ARCH}.tar.xz.sha256"; \
  sha256sum -c "s6-overlay-${S6_ARCH}.tar.xz.sha256"; \
  wget "${S6_OVERLAY_URL}/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz"; \
  wget "${S6_OVERLAY_URL}/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz.sha256"; \
  sha256sum -c "s6-overlay-noarch.tar.xz.sha256"; \
  wget "${S6_OVERLAY_URL}/v${S6_OVERLAY_VERSION}/syslogd-overlay-noarch.tar.xz"; \
  wget "${S6_OVERLAY_URL}/v${S6_OVERLAY_VERSION}/syslogd-overlay-noarch.tar.xz.sha256"; \
  sha256sum -c "syslogd-overlay-noarch.tar.xz.sha256"; \
  tar -C / -Jxpf "/tmp/s6/s6-overlay-noarch.tar.xz"; \
  tar -C / -Jxpf "/tmp/s6/s6-overlay-${S6_ARCH}.tar.xz"; \
  tar -C / -Jxpf "/tmp/s6/syslogd-overlay-noarch.tar.xz"; \
  rm -rf /tmp/s6;

ARG POSTFIX_ID=2525
ARG ZEYPLE_ID=7167

ARG ZEYPLE_GITURL="https://raw.github.com/infertux/zeyple/master"

ENV ZEYPLE_DIR="/var/lib/zeyple"
ENV ZEYPLE_CFG="/etc/zeyple.conf"
ENV POSTFIX_LOGDIR="/var/log/postfix"
ENV POSTFIX_CFGDIR="/etc/postfix"
ENV POSTFIX_SPOOLDIR="/var/spool/postfix"

RUN set -eux; \
  addgroup --system --gid "${ZEYPLE_ID}" zeyple; \
  adduser  --system --uid "${ZEYPLE_ID}" --ingroup zeyple \
    --no-create-home --home "${ZEYPLE_DIR}" \
    --disabled-password zeyple; \
  addgroup --system --gid "${POSTFIX_ID}" postfix; \
  adduser  --system --uid "${POSTFIX_ID}" --ingroup postfix \
    --no-create-home --home "${POSTFIX_SPOOLDIR}" \
    --disabled-password postfix;

RUN set -eux; \
  mkdir -p "${ZEYPLE_DIR}/keys" "${ZEYPLE_DIR}/import" "${POSTFIX_LOGDIR}"; \
  apt update && apt-get install -y --no-install-recommends \
    inotify-tools gnupg python3 python-is-python3 python3-gpg postfix libsasl2-modules; \
  apt-get clean; \
  postfix post-install create-missing; \
  cp -a "${POSTFIX_SPOOLDIR}" /var/spool/postfix.cache; \
  rm -rf /var/lib/apt/lists/* "${POSTFIX_CFGDIR}/main.cf" "${POSTFIX_CFGDIR}/master.cf"; \
  wget -q -O /usr/local/bin/zeyple.py "${ZEYPLE_GITURL}/zeyple/zeyple.py"; \
  wget -q -O "${ZEYPLE_DIR}/zeyple.conf.proto" "${ZEYPLE_GITURL}/zeyple/zeyple.conf.example"; \
  chmod 0555 "/usr/local/bin/zeyple.py"; \
  chmod 2750 "${ZEYPLE_DIR}/keys" "${ZEYPLE_DIR}/import"; \
  chown zeyple:zeyple "${ZEYPLE_DIR}/keys" "${ZEYPLE_DIR}/import"; \
  chown -R postfix:postfix "${POSTFIX_LOGDIR}";

VOLUME [ "${ZEYPLE_DIR}/keys" ]

VOLUME [ "${POSTFIX_SPOOLDIR}" ]

COPY s6-rc.d /etc/s6-overlay/s6-rc.d

COPY scripts /opt/scripts

EXPOSE 25 587

HEALTHCHECK --interval=1m --timeout=10s \
  CMD postfix status || exit 1

ENTRYPOINT ["/init"]
