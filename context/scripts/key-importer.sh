#!/bin/sh

export GNUPGHOME="${ZEYPLE_DIR}/keys"

cd "${ZEYPLE_DIR}"

key_import() {
    echo "New file: ${1}"
    if [ "${1##*.}" = "asc" ]; then
        gpg --import "${1}"
        echo "${1} imported to keyring"
    fi

    rm -f "${1}"
    echo "${1} Removed"
}

# Import public keys on start
if ls -1A "${ZEYPLE_DIR}/import" | grep -q .; then
	for FILE in "${ZEYPLE_DIR}/import/*.asc"; do
        key_import "${FILE}"
	done
fi

# Monitor and import on demand
inotifywait -qmre moved_to,create --format '%w%f' "${ZEYPLE_DIR}/import" |\
while read FILE; do
    key_import "${FILE}"
done