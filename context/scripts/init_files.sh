#!/bin/sh

if [ "${SHELL_DEBUG}" = "true" ]; then
	set -x
fi

set -e # terminate on errors

ZEYPLE_LOGFILE="/var/log/zeyple.log"

if [ ! -f "${ZEYPLE_CFG}" ]; then
	cp -fa "${ZEYPLE_DIR}/zeyple.conf.proto" "${ZEYPLE_CFG}"
fi

if [ ! -f "${ZEYPLE_LOGFILE}" ]; then
	touch ${ZEYPLE_LOGFILE}
fi

if [ ! -f "${POSTFIX_CFGDIR}/master.cf" ]; then
	cat "${POSTFIX_CFGDIR}/master.cf.proto" "/opt/scripts/zeyple_mastercf" >"${POSTFIX_CFGDIR}/master.cf"
fi

if [ ! -f "${POSTFIX_CFGDIR}/main.cf" ]; then
	POSTFIX_MAINCF="${POSTFIX_CFGDIR}/main.cf"

	touch "${POSTFIX_CFGDIR}/main.cf"

	: "${MSG_SIZE_LIMIT:=8388608}"
	# Set Cert & Key filepath
	: "${POSTFIX_CRT:=/etc/ssl/certs/ssl-cert-snakeoil.pem}"
	: "${POSTFIX_KEY:=/etc/ssl/private/ssl-cert-snakeoil.key}"

	# Default settings
	postconf -e \
		default_database_type='lmdb' \
		alias_database='lmdb:/etc/aliases' \
		alias_maps='lmdb:/etc/aliases' \
		append_dot_mydomain='no' \
		biff='no' \
		compatibility_level='2' \
		inet_protocols='ipv4' \
		inet_interfaces='all' \
		mailbox_size_limit='0' \
		message_size_limit="${MSG_SIZE_LIMIT}" \
		myorigin="${HOSTNAME}" \
		mydestination='$myhostname, $mydomain, $myorigin, localhost.localdomain, localhost' \
		readme_directory='no' \
		recipient_delimiter='+' \
		smtp_tls_CApath='/etc/ssl/certs' \
		smtp_tls_security_level='may' \
		smtp_tls_session_cache_database='lmdb:${data_directory}/smtp_scache' \
		smtpd_banner='$myhostname ESMTP $mail_name (Debian/GNU)' \
		smtpd_relay_restrictions='permit_mynetworks permit_sasl_authenticated defer_unauth_destination' \
		smtpd_tls_cert_file="${POSTFIX_CRT}" \
		smtpd_tls_key_file="${POSTFIX_KEY}"

	# Configure hostname and domain for postfix
	if [ ! -z "${POSTFIX_MYHOSTNAME}" ]; then
		postconf -e myhostname="${POSTFIX_MYHOSTNAME}"
	fi

	if [ ! -z "${POSTFIX_MYDOMAIN}" ]; then
		postconf -e mydomain="${POSTFIX_MYDOMAIN}"
	fi

	if [ ! -z "${POSTFIX_TLS}" ] && [ "${POSTFIX_TLS}" = "true" ]; then
		postconf -e smtpd_use_tls="yes" \
			smtpd_tls_security_level="encrypt"
	else
		postconf -e smtpd_use_tls="no"
	fi

	# configure relay
	if [ ! -z "${POSTFIX_RELAYHOST}" ]; then
		postconf -e relayhost="${POSTFIX_RELAYHOST}"

		if [ ! -z "${POSTFIX_RELAY_AUTH}" ] && [ "${POSTFIX_RELAY_AUTH}" = "true" ]; then
			postconf -e smtp_sasl_auth_enable="yes" \
				smtp_sasl_security_options="noanonymous"

			if [ ! -z "${POSTFIX_RELAY_TLS}" ] && [ "${POSTFIX_RELAY_TLS}" = "true" ]; then
				postconf -e smtp_use_tls='yes' \
					smtp_tls_security_level='encrypt'
			fi

			if [ -f "${POSTFIX_SASLPWD}" ]; then
				cat "${POSTFIX_SASLPWD}" >"${POSTFIX_CFGDIR}/sasl_passwd"
				postmap "${POSTFIX_CFGDIR}/sasl_passwd"
				postconf -e smtp_sasl_password_maps="lmdb:${POSTFIX_CFGDIR}/sasl_passwd"
			fi
		fi
	fi

	if [ ! -z "${POSTFIX_ALLOW_PRIVATE}" ] && [ "${POSTFIX_ALLOW_PRIVATE}" = "true" ]; then
		postconf -e mynetworks='127.0.0.0/8 [::1]/128 [::ffff:127.0.0.0]/104 192.168.0.0/16 172.16.0.0/12 10.0.0.0/8 [fe80::]/10 [fd00::]/8'
	else
		postconf -e mynetworks='127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128'
	fi

	postconf -e content_filter='zeyple'
fi

if [ -z "$(ls -A $POSTFIX_SPOOLDIR)" ]; then
	cp -a /var/spool/postfix.cache/* ${POSTFIX_SPOOLDIR}

	if [ ! -d "${POSTFIX_SPOOLDIR}/etc" ]; then
		mkdir "${POSTFIX_SPOOLDIR}/etc"
	fi
else
	# Fix spool directory permissions
	chgrp -R postdrop ${POSTFIX_SPOOLDIR}/public
	chgrp -R postdrop ${POSTFIX_SPOOLDIR}/maildrop
fi

cp -af /etc/resolv.conf "${POSTFIX_SPOOLDIR}/etc/resolv.conf"

if [ ! -f "/etc/aliases" ]; then
	touch /etc/aliases
fi

# Fix Permissions
{
	chmod 2750 -R ${ZEYPLE_DIR}
	chmod 0700 -R "${ZEYPLE_DIR}/keys"
	chmod 2777 -R "${ZEYPLE_DIR}/import"
	chmod 0555 /opt/scripts/key-importer.sh
	chown zeyple:zeyple -R ${ZEYPLE_DIR} ${ZEYPLE_LOGFILE} /opt/scripts/key-importer.sh
	postfix set-permissions || true
}
